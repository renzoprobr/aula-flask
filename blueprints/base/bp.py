from flask import Blueprint, Flask

bp = Blueprint('base', __name__)


@bp.route('/')
def hello():
    return 'Olá Mundo'


def init_app(app:Flask, url_prefix=''):
    app.register_blueprint(bp)
