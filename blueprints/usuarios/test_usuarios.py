from blueprints.usuarios.models import Usuario


def test_salvar_usuario(client, db_session):
    resp = client.get('/usuarios/Renzo')
    assert resp.status_code == 200


def test_salvar_usuario_model(client, db_session):
    client.get('/usuarios/Renzo')
    assert Usuario.query.count() == 1


def test_session_access(flask_session):
    flask_session['a_key'] = 'a value'
    assert flask_session['a_key'] == 'a value'
