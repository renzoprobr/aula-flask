from ext.db import db


class Usuario(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(64), nullable=False)

    def __repr__(self):
        return f'Usuario(id={self.id}, nome={self.nome})'
