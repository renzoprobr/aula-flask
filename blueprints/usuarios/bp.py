from flask import Blueprint, Flask

from blueprints.usuarios.models import Usuario
from ext.db import db

bp = Blueprint('usuarios', __name__)


@bp.route('/<string:nome>')
def ola(nome):
    usuario = Usuario(nome=nome)
    db.session.add(usuario)
    db.session.commit()
    return f'Olá {usuario}'


@bp.route('/listar')
def listar():
    usuarios = Usuario.query.order_by(Usuario.nome).all()
    return str(usuarios)


def init_app(app: Flask, url_prefix='/usuarios'):
    app.register_blueprint(bp, url_prefix=url_prefix)
