from flask import Flask

import settings as default_settings
from blueprints.base import bp as base_bp
from blueprints.usuarios import bp as usuarios_bp
from ext import migrate, celery_ext
from ext.db import db


def create_app(settings=None) -> Flask:
    app = Flask(__name__)
    if None:
        settings = default_settings

    app.config.from_object(settings)

    # Extensões
    # db.init_app(app)
    migrate.init_app(app, db)

    # Blueprints
    usuarios_bp.init_app(app)
    usuarios_bp.init_app(app, '/users')

    base_bp.init_app(app)
    celery_ext.init_app(app)

    return app
